<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    protected $table = 'news';

    /**
     * @var array
     */
    protected $fillable = [
        'author',
        'title',
        'description',
        'url',
        'urlToImage',
        'publishedAt',
        'content'
    ];

    public $timestamps = false;

    /**
     * Get the source record associated with the news.
     */
    public function source()
    {
        return $this->hasMany(Source::class);
    }

}
