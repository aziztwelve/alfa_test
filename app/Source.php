<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $table = 'sources';

    /**
     * @var array
     */
    protected $fillable = [
        'source_id',
        'source_name',
        'url',
        'news_id'
    ];

    public $timestamps = false;


    /**
     * Get the news that owns the source.
     */
    public function news()
    {
        return $this->belongsTo(News::class);
    }
}
