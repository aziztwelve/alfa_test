<?php


namespace App\Jobs;


use App\Services\News;

class SaveNewsJob extends Job
{

    protected $random;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->random = News::random();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        News::save($this->random)->delay(now()->addMinutes(1));
    }

}
