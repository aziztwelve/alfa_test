<?php


namespace App\Http\Controllers;


use App\News as NewsModel;
use App\Services\News;
use Illuminate\Http\JsonResponse;

class NewsControllers extends Controller
{

    /**
     * @param string $source
     * @return \Illuminate\Http\JsonResponse
     */
    public function showBySource(string $source): JsonResponse
    {
        $news_by_source = NewsModel::whereHas(
            'source',  function ($query)  use ($source) {
                $query->where('source_name', 'like', '%'. $source . '%');
            }
        )->with('source')->get();

        return response()->json($news_by_source);
    }


    /**
     * @param int $date
     * @return JsonResponse
     */
    public function showByDate(int $date): JsonResponse
    {
        $news_by_date = NewsModel::whereDay('publishedAt', '=', $date )->with('source')->get();

        return response()->json($news_by_date);
    }


    /**
     * @param string $category
     * @return JsonResponse
     */
    public function showByCategory(string $category): JsonResponse
    {
        $news_by_category = NewsModel::where('title', 'like', '%'. $category . '%')->with('source')->get();

        return response()->json($news_by_category);
    }

}
