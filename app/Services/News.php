<?php


namespace App\Services;


use App\News as NewsModel;
use App\Source;

class News
{

    public static function random(): string
    {
        $arr = ['bitcoun', 'litecoin', 'ripple', 'dash', 'ethereum' ];
        $k = array_rand($arr);
        return $arr[$k];
    }

    public static function save(string $search):void
    {
        $json = json_decode(file_get_contents("https://newsapi.org/v2/everything?q=". $search ."&apiKey=". env('NEWS_API') .""), true);
        $one_news = array_rand($json['articles']);

        $news_id = NewsModel::create([
            'author' => $json['articles'][$one_news]['author'] ?? "",
            'title' => $json['articles'][$one_news]['title'],
            'description' => $json['articles'][$one_news]['description'],
            'url' => $json['articles'][$one_news]['url'],
            'urlToImage' => $json['articles'][$one_news]['urlToImage'] ?? "",
            'publishedAt' => $json['articles'][$one_news]['publishedAt'],
            'content' => $json['articles'][$one_news]['content'] ?? "",
        ])->id;
        Source::create([
            'news_id' => (int)$news_id,
            'source_id' => $json['articles'][$one_news]['source']['id'] ?? "",
            'url' => $json['articles'][$one_news]['url'],
            'source_name' => $json['articles'][$one_news]['source']['name'],
        ]);
    }

}
