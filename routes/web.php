<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Queue;
use App\Jobs\SaveNewsJob;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/news/create', function () {
    Queue::push(new SaveNewsJob());
});


$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('news/source/{source}', ['uses' => 'NewsControllers@showBySource']);

    $router->get('news/date/{date}', ['uses' => 'NewsControllers@showByDate']);

    $router->get('news/category/{category}', ['uses' => 'NewsControllers@showByCategory']);

});
